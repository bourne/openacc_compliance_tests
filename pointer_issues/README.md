The OpenACC routine directive doesn't handle the use of pointer to Derived-Type, while it works for pointer to "simple" types (real or integer arrays).
