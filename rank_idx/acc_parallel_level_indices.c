#ifdef __NVCOMPILER
#include "openacc.h"

#pragma acc routine seq
int get_gang_id() {
    return __pgi_gangidx() + 1;
}

#pragma acc routine seq
int get_worker_id() {
    return __pgi_workeridx() + 1;
}

#pragma acc routine seq
int get_vector_id() {
    return __pgi_vectoridx() + 1;
}
#endif
