# OpenACC Compliance Tests

A bank of tests to determine what we can and can't do with the version of OpenACC installed on this system.

## How to run tests

The tests use cmake and ctest. To run the tests you need to do:
```
mkdir build
cd build
cmake ..
make
ctest
```

The tests will be listed and will pass when OpenACC behaves as expected and fail otherwise.
