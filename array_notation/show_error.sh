
echo "--------- Compiling with nvfortran ------------"

echo "nvfortran -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 mod.F90"
nvfortran -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 mod.F90
echo "nvfortran -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 main.F90"
nvfortran -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 main.F90
echo "nvfortran -O3 mod.o main.o -o main"
nvfortran -O3 mod.o main.o -o main

echo "--------- Compiling with mpif90 ------------"

echo "mpif90 -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 mod.F90"
mpif90 -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 mod.F90
echo "mpif90 -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 main.F90"
mpif90 -cpp -g -g -c -O3 -Mnoopenmp -Minfo=all  -r8 main.F90
echo "mpif90 -O3 mod.o main.o -o main"
mpif90 -O3 mod.o main.o -o main
