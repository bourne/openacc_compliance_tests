program my_prog

  use my_mod, only: create_pointer, free_pointer, assign_pointer

  call create_pointer(1000,20)
  call assign_pointer()
  call free_pointer()

end program my_prog
