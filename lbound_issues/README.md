

The OpenACC routine directive doesn't handle the lbound of a pointer correctly.

In this reproducer a function `fill_array` marked with:
```
!$acc routine seq
```
is called inside a loop. This function takes a pointer or allocatable in order to inherit the lbound.

When compiling with OpenACC the lbound is not inherited leading to the following error:
```
Failing in Thread:1
call to cuStreamSynchronize returned error 719: Launch failed (often invalid pointer dereference)
```

The reproducer presents 6 scenarios. 3 with pointers and 3 with allocatables:

Results on V100, nvfortran v21.2.0:
|-----------------------|-----------|---------------|
|                       |  Pointer  |  Allocatable  |
|-----------------------|-----------|---------------|
| No OpenACC            |    OK     |      OK       |
| With OpenACC          |    KO     |      KO       |
| Inlining `fill_array` |    OK     |      OK       |
|-----------------------|-----------|---------------|


Results on A100, nvfortran v23.1.0:
|-----------------------|-----------|---------------|
|                       |  Pointer  |  Allocatable  |
|-----------------------|-----------|---------------|
| No OpenACC            |    OK     |      OK       |
| With OpenACC          |    OK     |      OK       |
| Inlining `fill_array` |    OK     |      OK       |
|-----------------------|-----------|---------------|


Results on A100, nvfortran v23.3.0:
|-----------------------|-----------|---------------|
|                       |  Pointer  |  Allocatable  |
|-----------------------|-----------|---------------|
| No OpenACC            |    OK     |      OK       |
| With OpenACC          |    OK     |      OK       |
| Inlining `fill_array` |    OK     |      OK       |
|-----------------------|-----------|---------------|

