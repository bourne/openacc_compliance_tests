According to the standard:
> (J3/18-007r1) 8.5.10 (2) “INTENT attribute”
> 
> The INTENT (IN) attribute for a pointer dummy argument specifies that during the invocation and execution of the procedure its association shall not be changed except that it may become undefined if the target is deallocated other than through the pointer (19.5.2.5).

However the compiler does not respect this.

## Results:

v23.1 : Warning raised (remains warning with Werror)

v23.3 : Warning raised (remains warning with Werror)
