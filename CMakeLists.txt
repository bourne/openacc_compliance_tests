cmake_minimum_required(VERSION 3.17)

# Creating label for project
project(openacc_testing LANGUAGES C CXX Fortran)

find_package(OpenACC REQUIRED)

include(CTest)

add_subdirectory(lbound_issues)
add_subdirectory(pointer_issues)
add_subdirectory(derivedType_argument_issues)
add_subdirectory(slice_issues)
add_subdirectory(slice_issues_2)
add_subdirectory(const_pointer)

#add_subdirectory(rank_idx)
