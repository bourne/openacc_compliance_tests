
message(STATUS "slices")

list(APPEND slice_type "IMPLICIT_SLICE" "EXPLICIT_SLICE" "LBOUND")
list(APPEND routine_levels "seq" "vector" "worker")

foreach(SLICE IN LISTS slice_type)
	foreach(LEVEL IN LISTS routine_levels)
		message(STATUS "Compiling ${ARRAY_TYPE}")

		set(CURRENT_TARGET_NAME slice_${SLICE}_${LEVEL})
		set(FFLAGS -Minfo -D${SLICE} -DROUTINE_LEVEL=${LEVEL})

		add_executable(${CURRENT_TARGET_NAME}
			type_exp.F90
			main.F90)

		set_target_properties(${CURRENT_TARGET_NAME}
			PROPERTIES
			Fortran_MODULE_DIRECTORY
			${CMAKE_CURRENT_BINARY_DIR}/${CURRENT_TARGET_NAME}_mod)

		target_compile_options(${CURRENT_TARGET_NAME}
			PUBLIC ${FFLAGS})

		target_compile_options(${CURRENT_TARGET_NAME}
			PRIVATE ${OpenACC_Fortran_OPTIONS})
		target_link_libraries(${CURRENT_TARGET_NAME}
			PRIVATE ${OpenACC_Fortran_OPTIONS})

		add_test(NAME ${CURRENT_TARGET_NAME} COMMAND ${CURRENT_TARGET_NAME})

	endforeach()
	set(CURRENT_TARGET_NAME slice_${SLICE}_serial)
	set(FFLAGS -Minfo -D${SLICE} -DROUTINE_LEVEL=${LEVEL})

	add_executable(${CURRENT_TARGET_NAME}
		type_exp.F90
		main.F90)

	set_target_properties(${CURRENT_TARGET_NAME}
		PROPERTIES
		Fortran_MODULE_DIRECTORY
		${CMAKE_CURRENT_BINARY_DIR}/${CURRENT_TARGET_NAME}_mod)

	target_compile_options(${CURRENT_TARGET_NAME}
		PUBLIC ${FFLAGS})

endforeach()
